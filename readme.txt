
Task : 

Part I - Github API & Pipedrive API
Using the Github API you should be able to query a user’s publicly available GitHub gists and create a deal/activity in Pipedrive for each gist. Implement an application that periodically checks for a user's publicly available gists, this application should also have a web endpoint where it should show the gists for that user that were added since the last visit.

Expectations:
You can use any programming language you want
No need to get crazy with HTML/CSS or JSON APIs, a simple text response will suffice
This problem description is intentionally vague, document your assumptions and justify your decisions
Good output, clear logging and let us know what it's doing
README file with clear instructions on how to run the application on our local computers
Implement an endpoint that shows all users that have been scanned.

Part II - CI /CD
For preparation for the next part (The Cloud), it is necessary to set up a CI /CD process. For public repositories exists several tools that allow CI/CD processes (Travis, Codeship, Google Cloud Build)
Expectations:
Less manual work better
Run the checks that you think it's necessary or it brings value to the delivery's software process
Bonus points:
It’s triggered by commit

Part III - The cloud
Let's get that application that you've done onto the cloud. Periodic checks should run every 3 hours.
Expectations:
You can use any provisioning tool you want
The solution should be:
Dockerized
Resilient
Scalable
You can use any cloud provider you'd like
You should be able to complete this challenge with the free tier of any major cloud provider and you'll have to show us the deployed application on the web. For example, Google Cloud gives free tier https://cloud.google.com/free/
Bonus points:
Security

Part IV - Monitoring
Now we have the application in the cloud. Let's start measuring the response time on each API method. The goal here is for you to find some open-source tool (Prometheus or elastic metricbeats docker images for example), instrument your code, and create a simple dashboard where we see some metrics for our endpoints.
Expectations:
Keep as much configuration as code as possible.
The monitoring tool is available from the local machine
Bonus points:
Integrate your monitoring solution with a visualization tool (Grafana or Kibana)
Monitoring tool and visualization tool are available from the cloud




solution ::


1. clone git repo : git clone git@bitbucket.org:akshay-shetty/pipedrive-task.git
2. docker-compose build 
3. docker-compose up
4. open localhost:8001/api/login to use the login page 
5. open localhost:8001/api/signup to enter the gist-id, password and your pipedrive api-token
6. sample gists : agustin360 
7. you can see the activity created in pipedrive CRM as spicified in the task. 
8. for the pipeline and deployment to google cloud, check out the bitbucket-pipelines.yml file 
9. the pipeline is triggered by every commit and deploys to google cloud acutomatically,
10. prometheus is running on localhost:9090
11. grafanna is running on localhost:3000
12. 